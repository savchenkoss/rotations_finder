#!/usr/bin/env python

import json
import argparse
import os
from itertools import pairwise
from pathlib import Path
import numpy as np
from scipy.stats import binomtest
from scipy import odr
from scipy import stats

import matplotlib.pyplot as plt
from astropy.stats import bayesian_blocks
from astropy.stats import sigma_clip
from astropy.time import Time
import matplotlib


matplotlib.rcParams['text.usetex'] = False
matplotlib.use("Qt5Agg")
allowed_sources = ["azt-8+st7", "perkins", "lx-200", "steward", "robopol"]


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def linear_model(B, x):
    return B[0]*x + B[1]


def fit_by_line(x, y, sy=None):
    # Find initial conditions for ODR by scipy.stats.linregress
    slope0, intercept0, *_ = stats.linregress(x, y)
    # Perform ODR linear fit
    if sy is not None:
        data = odr.RealData(x=x, y=y, sy=sy)
    else:
        data = odr.Data(x, y)
    results = odr.ODR(data=data, model=odr.Model(linear_model), beta0=[slope0, intercept0]).run()
    return results


class PolarizationChunk:
    def __init__(self, times, magnitude_values, magnitude_sigmas, degree_values,
                 degree_sigmas, evpa_values, evpa_sigmas, kinematics):
        print(f"New chunk: t_start={times[0]} t_end={times[-1]}")
        self.times = times
        self.magnitude_values = magnitude_values
        self.magnitude_sigmas = magnitude_sigmas
        self.degree_values = degree_values
        self.degree_sigmas = degree_sigmas
        self.evpa_values = evpa_values
        self.evpa_sigmas = evpa_sigmas
        self.kinematics = kinematics
        self.first_sub_chunk = None
        self.second_sub_chunk = None
        # Make sure that the first point is inside 1:180 degrees range
        shift = 180 * (self.evpa_values[0] // 180)
        self.evpa_values -= shift
        # Resolve +/- 180 degrees ambiguity
        self.evpa_shift_times = []
        for idx in range(1, len(self.evpa_values)):
            evpa_delta = self.evpa_values[idx] - self.evpa_values[idx-1]
            evpa_sigma_delta = (self.evpa_sigmas[idx]**2 + self.evpa_sigmas[idx-1]**2)**0.5
            if evpa_delta + evpa_sigma_delta < -90:
                self.evpa_values[idx:] += 180
                self.evpa_shift_times.append((times[idx], 1))
            elif evpa_delta - evpa_sigma_delta > 90:
                self.evpa_values[idx:] -= 180
                self.evpa_shift_times.append((times[idx], -1))

        # Compute relative stokes parameters
        evpa_rad = np.radians(self.evpa_values)
        evpa_rad_sigmas = np.radians(self.evpa_sigmas)
        self.q_values = self.degree_values * np.cos(2*evpa_rad)
        self.q_sigmas = np.sqrt((np.cos(2*evpa_rad) * self.degree_sigmas)**2 +
                                (2*self.degree_values * np.sin(2*evpa_rad) * evpa_rad_sigmas)**2)
        self.u_values = self.degree_values * np.sin(2*evpa_rad)
        self.u_sigmas = np.sqrt((np.sin(2*evpa_rad) * self.degree_sigmas)**2 +
                                (2*self.degree_sigmas * np.cos(2*evpa_rad) * evpa_rad_sigmas)**2)
        self.detected_rotations_inds = None
        self.detected_rotations_directions = None
        # Compute EVPA rotation rates
        self.evpa_rates = np.diff(self.evpa_values) / np.diff(self.times)
        self.evpa_rate_sigmas = []
        for idx in range(1, len(self.evpa_values)):
            s1 = self.evpa_sigmas[idx-1]
            s2 = self.evpa_sigmas[idx]
            dt = self.times[idx] - self.times[idx-1]
            sigma_r = (s1**2 + s2**2) ** 0.5 / dt
            self.evpa_rate_sigmas.append(sigma_r)
        self.evpa_rate_sigmas = np.array(self.evpa_rate_sigmas)

        # Setup some internal variables
        self.selected_region_end_pos = None
        self.selected_region_start_pos = None
        self.t_test_p_value = None
        self.binom_test_p_value = None

        # Handlers for saved rotations
        self.saved_rotations_edges = []
        self.saved_rotations_stats = []

    def setup_plots(self, objname):
        # Setup plots
        self.figure, (self.ax_degree, self.ax_evpa, self.ax_q, self.ax_u) = plt.subplots(4, 1, sharex=True,
                                                                                         figsize=(5, 7.5))
        self.ax_degree.set_title("Polarization degree")
        self.ax_degree.set_ylabel("$p$ %")
        self.ax_evpa.set_title("EVPA")
        self.ax_evpa.set_ylabel(r"$\chi$ [degrees]")
        self.ax_q.set_title("q")
        self.ax_q.set_ylabel("$q$ %")
        self.ax_u.set_title("u")
        self.ax_u.set_ylabel("$u$ %")
        self.figure.canvas.mpl_connect('button_press_event', self.on_mouse_click)
        self.figure.canvas.mpl_connect('button_release_event', self.on_mouse_release)
        self.figure.canvas.mpl_connect('key_press_event', self.on_press)
        self.figure.canvas.manager.set_window_title(objname)
        # Removable plot instances
        self.selected_region_plot_instance = None
        self.edges_plot_instances = []
        self.split_plot_instance = None
        self.appended_selections_plot_instances = []
        self.skipped_points_plot_instances = []

    def split(self, t_click):
        """
        Split time chunk onto two separate chunks at a given time
        """
        # Find closest block edge
        t_split = [e[0] for e in self.block_edges if e[0] < t_click][-1]
        # Find indices of two halfs
        inds_first = np.where(self.times <= t_split)
        inds_second = np.where(self.times > t_split)
        self.first_sub_chunk = PolarizationChunk(times=self.times[inds_first],
                                                 magnitude_values=self.magnitude_values[inds_first],
                                                 magnitude_sigmas=self.magnitude_sigmas[inds_first],
                                                 degree_values=self.degree_values[inds_first],
                                                 degree_sigmas=self.degree_sigmas[inds_first],
                                                 evpa_values=self.evpa_values[inds_first],
                                                 evpa_sigmas=self.evpa_sigmas[inds_first],
                                                 kinematics=self.kinematics)
        self.second_sub_chunk = PolarizationChunk(times=self.times[inds_second],
                                                  magnitude_values=self.magnitude_values[inds_second],
                                                  magnitude_sigmas=self.magnitude_sigmas[inds_second],
                                                  degree_values=self.degree_values[inds_second],
                                                  degree_sigmas=self.degree_sigmas[inds_second],
                                                  evpa_values=self.evpa_values[inds_second],
                                                  evpa_sigmas=self.evpa_sigmas[inds_second],
                                                  kinematics=self.kinematics)
        # Draw split location
        if self.split_plot_instance is not None:
            self.split_plot_instance.remove()
            self.split_plot_instance = None
        self.split_plot_instance = self.ax_evpa.axvline(x=t_split, color="c")
        self.figure.canvas.draw()

    def on_press(self, event):
        """ Handle keyboard buttons pressed """
        if event.key == "a":
            self.append_selection()

    def on_mouse_click(self, event):
        if event.button == 1:
            self.on_mouse_left_click(event)
        elif event.button == 3:
            self.on_mouse_right_click(event)

    def on_mouse_release(self, event):
        if event.button == 1:
            self.on_mouse_left_release(event)

    def on_mouse_left_click(self, event):
        if self.figure.canvas.cursor().shape() != 0:
            # Some instrument was selected on a toolbar, we do not want to interfere with it
            return
        if event.inaxes != self.ax_evpa:
            # A click was not made on an EVPA subplot
            return
        self.selected_region_start_pos = event.xdata

    def on_mouse_left_release(self, event):
        if self.figure.canvas.cursor().shape() != 0:
            # Some instrument was selected on a toolbar, we do not want to interfere with it
            return
        self.selected_region_end_pos = event.xdata
        if (self.selected_region_end_pos is None) or (self.selected_region_start_pos is None):
            return
        if self.selected_region_end_pos <= self.selected_region_start_pos:
            self.selected_region_end_pos = None
            self.selected_region_start_pos = None
            return
        self.draw_selected_region()
        self.investigate_selected_region_bb()

    def on_mouse_right_click(self, event):
        if self.figure.canvas.cursor().shape() != 0:
            # Some instrument was selected on a toolbar, we do not want to interfere with it
            return
        if event.inaxes != self.ax_evpa:
            # A click was not made on an EVPA subplot
            return
        self.split(event.xdata)

    def draw_selected_region(self):
        if self.selected_region_start_pos is None:
            return
        if self.selected_region_plot_instance is not None:
            self.selected_region_plot_instance.remove()
            self.selected_region_plot_instance = None
        self.selected_region_plot_instance = self.ax_evpa.axvspan(self.selected_region_start_pos,
                                                                  self.selected_region_end_pos,
                                                                  alpha=0.5, color='red')
        self.figure.canvas.draw()

    def append_selection(self):
        """
        Append selected region to a list of rotations
        """
        if (self.selected_region_start_pos is None) or (self.selected_region_end_pos is None) or (self.t_test_p_value is None):
            return
        block_starts = np.array([e[0] for e in self.block_edges])
        block_ends = np.array([e[1] for e in self.block_edges])
        t_start = max(block_starts[block_starts < self.selected_region_start_pos])
        t_end = min(block_ends[block_ends > self.selected_region_end_pos])
        self.saved_rotations_edges.append((t_start, t_end))
        self.saved_rotations_stats.append({"t_test_p_value": self.t_test_p_value,
                                           "binom_test_p_value": self.binom_test_p_value})
        while self.appended_selections_plot_instances:
            self.appended_selections_plot_instances.pop().remove()
        for t_start, t_end in self.saved_rotations_edges:
            p = self.ax_evpa.axvspan(t_start, t_end, alpha=0.25, color="k")
            self.appended_selections_plot_instances.append(p)
        self.figure.canvas.draw()

    def save_selection_to_file(self, selection_idx, fname):
        t_start, t_end = self.saved_rotations_edges[selection_idx]
        data_to_save = self.saved_rotations_stats[selection_idx].copy()
        # Save individual points of the rotation
        inds = np.where((self.times >= t_start) * (self.times < t_end+0.0000001))
        data_to_save["times"] = list(self.times[inds])
        magnitudes = self.magnitude_values[inds]
        magnitudes[np.logical_not(np.isfinite(magnitudes))] = 99.9
        data_to_save["magnitude_values"] = list(magnitudes)
        magnitude_sigmas = self.magnitude_sigmas[inds]
        magnitude_sigmas[np.logical_not(np.isfinite(magnitude_sigmas))] = 99.9
        data_to_save["magnitude_sigmas"] = list(magnitude_sigmas)
        data_to_save["degree_values"] = list(self.degree_values[inds])
        data_to_save["degree_sigmas"] = list(self.degree_sigmas[inds])
        data_to_save["evpa_values"] = list(self.evpa_values[inds])
        data_to_save["evpa_sigmas"] = list(self.evpa_sigmas[inds])
        data_to_save["q_values"] = list(self.q_values[inds])
        data_to_save["q_sigmas"] = list(self.q_sigmas[inds])
        data_to_save["u_values"] = list(self.u_values[inds])
        data_to_save["u_sigmas"] = list(self.u_sigmas[inds])
        # Save full chunk for a context
        data_to_save["times_full"] = list(self.times)
        magnitudes = self.magnitude_values
        magnitudes[np.logical_not(np.isfinite(magnitudes))] = 99.9
        data_to_save["magnitude_values_full"] = list(magnitudes)
        magnitude_sigmas = self.magnitude_sigmas
        magnitude_sigmas[np.logical_not(np.isfinite(magnitude_sigmas))] = 99.9
        data_to_save["magnitude_sigmas_full"] = list(magnitude_sigmas)
        data_to_save["degree_values_full"] = list(self.degree_values)
        data_to_save["degree_sigmas_full"] = list(self.degree_sigmas)
        data_to_save["evpa_values_full"] = list(self.evpa_values)
        data_to_save["evpa_sigmas_full"] = list(self.evpa_sigmas)
        data_to_save["q_values_full"] = list(self.q_values)
        data_to_save["q_sigmas_full"] = list(self.q_sigmas)
        data_to_save["u_values_full"] = list(self.u_values)
        data_to_save["u_sigmas_full"] = list(self.u_sigmas)
        # Save bayesian blocks of a rotation
        inds = np.where((self.block_edges[:, 1] > t_start) * (self.block_edges[:, 0] < t_end))[0]
        data_to_save["block_starts"] = list(self.block_edges[inds, 0])
        data_to_save["block_ends"] = list(self.block_edges[inds, 1])
        data_to_save["block_centers"] = list(self.block_centers[inds])
        data_to_save["block_magnitude_values"] = list(self.block_magnitude_values[inds])
        data_to_save["block_magnitude_sigmas"] = list(self.block_magnitude_sigmas[inds])
        data_to_save["block_degree_values"] = list(self.block_degree_values[inds])
        data_to_save["block_degree_sigmas"] = list(self.block_degree_sigmas[inds])
        data_to_save["block_evpa_values"] = list(self.block_evpa_values[inds])
        data_to_save["block_evpa_sigmas"] = list(self.block_evpa_sigmas[inds])
        data_to_save["block_q_values"] = list(self.block_q_values[inds])
        data_to_save["block_q_sigmas"] = list(self.block_q_sigmas[inds])
        data_to_save["block_u_values"] = list(self.block_u_values[inds])
        data_to_save["block_u_sigmas"] = list(self.block_u_sigmas[inds])
        # Save full bayesian blocks for a context
        data_to_save["block_starts_full"] = [e[0] for e in self.block_edges]
        data_to_save["block_ends_full"] = [e[1] for e in self.block_edges]
        data_to_save["block_centers_full"] = list(self.block_centers)
        data_to_save["block_magnitude_values_full"] = list(self.block_magnitude_values)
        data_to_save["block_magnitude_sigmas_full"] = list(self.block_magnitude_sigmas)
        data_to_save["block_degree_values_full"] = list(self.block_degree_values)
        data_to_save["block_degree_sigmas_full"] = list(self.block_degree_sigmas)
        data_to_save["block_evpa_values_full"] = list(self.block_evpa_values)
        data_to_save["block_evpa_sigmas_full"] = list(self.block_evpa_sigmas)
        data_to_save["block_q_values_full"] = list(self.block_q_values)
        data_to_save["block_q_sigmas_full"] = list(self.block_q_sigmas)
        data_to_save["block_u_values_full"] = list(self.block_u_values)
        data_to_save["block_u_sigmas_full"] = list(self.block_u_sigmas)
        # Save information about outflows
        rot_center = 0.5 * (self.times[0] + self.times[-1])
        best_before = None
        best_before_dt = None
        best_after = None
        best_after_dt = None
        for i in range(len(self.kinematics)):
            t = self.kinematics[i][0]
            if (t < rot_center) and ((best_before is None) or (t > best_before)):
                best_before = t
                best_before_dt = self.kinematics[i][1]
            if (t > rot_center) and ((best_after is None) or (t < best_after)):
                best_after = t
                best_after_dt = self.kinematics[i][1]

        data_to_save["closest_ejection_before"] = best_before
        data_to_save["closest_ejection_before_dt"] = best_before_dt
        data_to_save["closest_ejection_after"] = best_after
        data_to_save["closest_ejection_after_dt"] = best_after_dt
        if best_before is not None:
            data_to_save["closest_ejection_before_distance"] = rot_center - best_before
        else:
            data_to_save["closest_ejection_before_distance"] = None
        if best_after is not None:
            data_to_save["closest_ejection_after_distance"] = best_after - rot_center
        else:
            data_to_save["closest_ejection_after_distance"] = None
        # Find various statistics
        widths = np.array([e[1]-e[0] for e in self.block_edges[inds]])
        mean_degree = np.average(self.block_degree_values[inds], weights=widths)
        evpa_rates = np.diff(self.block_evpa_values[inds]) / np.diff(self.block_centers[inds])
        smoothness = np.mean(np.abs(evpa_rates-np.mean(evpa_rates)))
        amplitude = self.block_evpa_values[inds][-1] - self.block_evpa_values[inds][0]
        time_span = self.block_centers[inds][-1] - self.block_centers[inds][0]
        data_to_save["mean_degree"] = mean_degree
        data_to_save["mean_rate"] = amplitude / time_span
        data_to_save["smoothness"] = smoothness
        data_to_save["amplitude"] = amplitude

        with open(fname, "w") as fout:
            json.dump(data_to_save, fout)

    def __len__(self):
        return len(self.times)

    def show(self):
        self.figure.tight_layout()
        self.figure.show()

    def compute_blocks(self, p0):
        edges = bayesian_blocks(self.times, self.evpa_values, self.evpa_sigmas,
                                fitness='measures', p0=p0)
        # edges = FitnessFunc(p0).fit(self.times, self.evpa_values, self.evpa_sigmas)
        edges[-1] += 0.0001
        self.block_edges = np.zeros((len(edges)-1, 2))
        self.block_centers = np.zeros(len(edges)-1)
        m0 = 25
        flux_values = 10**(0.4 * (m0-self.magnitude_values))
        flux_sigmas = 0.4 * np.log(10) * flux_values * self.magnitude_sigmas
        flux_weights = 1 / flux_sigmas**2
        self.block_magnitude_values = np.zeros(len(edges)-1)
        self.block_magnitude_sigmas = np.zeros(len(edges)-1)
        self.block_degree_values = np.zeros(len(edges)-1)
        self.block_degree_sigmas = np.zeros(len(edges)-1)
        self.block_evpa_values = np.zeros(len(edges)-1)
        self.block_evpa_sigmas = np.zeros(len(edges)-1)
        self.block_q_values = np.zeros(len(edges)-1)
        self.block_q_sigmas = np.zeros(len(edges)-1)
        self.block_u_values = np.zeros(len(edges)-1)
        self.block_u_sigmas = np.zeros(len(edges)-1)
        degree_weights = 1 / self.degree_sigmas**2
        evpa_weights = 1 / self.evpa_sigmas**2
        q_weights = 1 / self.q_sigmas**2
        u_weights = 1 / self.u_sigmas**2
        for idx, (edge_start, edge_end) in enumerate(pairwise(edges)):
            inds = np.where((self.times >= edge_start) * (self.times < edge_end))
            mag_inds = np.where((self.times >= edge_start) * (self.times < edge_end) * np.isfinite(flux_values))
            if len(inds[0]) == 0:
                continue
            self.block_edges[idx] = (edge_start, edge_end)
            self.block_centers[idx] = 0.5 * (edge_start + edge_end)
            self.block_degree_values[idx] = np.average(self.degree_values[inds], weights=degree_weights[inds])
            if len(mag_inds[0]) > 0:
                block_flux_value = np.average(flux_values[mag_inds], weights=flux_weights[mag_inds])
                block_flux_sigma = np.sqrt(1/np.sum(flux_weights[mag_inds]))
            else:
                block_flux_value = np.nan
                block_flux_sigma = np.nan
            self.block_magnitude_values[idx] = -2.5 * np.log10(block_flux_value) + m0
            self.block_magnitude_sigmas[idx] = 2.5 * block_flux_sigma / (np.log(10) * block_flux_value)
            self.block_degree_sigmas[idx] = np.sqrt(1/np.sum(degree_weights[inds]))
            self.block_evpa_values[idx] = np.average(self.evpa_values[inds], weights=evpa_weights[inds])
            self.block_evpa_sigmas[idx] = np.sqrt(1/np.sum(evpa_weights[inds]))
            self.block_q_values[idx] = np.average(self.q_values[inds], weights=q_weights[inds])
            self.block_q_sigmas[idx] = np.sqrt(1/np.sum(q_weights[inds]))
            self.block_u_values[idx] = np.average(self.u_values[inds], weights=u_weights[inds])
            self.block_u_sigmas[idx] = np.sqrt(1/np.sum(u_weights[inds]))
        # Find the direction of evpa in this block
        self.block_evpa_directions = np.zeros(len(edges)-1)
        for idx in range(len(self.block_evpa_values)):
            if self.block_evpa_values[idx] > self.block_evpa_values[idx-1]:
                self.block_evpa_directions[idx] = 1
            else:
                self.block_evpa_directions[idx] = -1
        print(f"Computed {len(self.block_edges)} Bayessian blocks out of {len(self.degree_values)} original points")

    def clean_plots(self):
        if self.selected_region_plot_instance is not None:
            self.selected_region_plot_instance.remove()
            self.selected_region_plot_instance = None
        while self.edges_plot_instances:
            self.edges_plot_instances.pop().remove()
        while self.skipped_points_plot_instances:
            self.skipped_points_plot_instances.pop().remove()

    def plot_skipped(self, skipped_centers, skipped_evpa):
        while self.skipped_points_plot_instances:
            self.skipped_points_plot_instances.pop().remove()
        p = self.ax_evpa.plot(skipped_centers, skipped_evpa, "rx")
        self.skipped_points_plot_instances.extend(p)
        self.figure.canvas.draw()

    def plot_data(self):
        self.ax_degree.errorbar(self.times, self.degree_values, yerr=self.degree_sigmas, marker="o",
                                color="0.5", markersize=1.5, linestyle="")
        self.ax_degree.set_xlim(self.times[0], self.times[-1])
        self.ax_evpa.errorbar(self.times, self.evpa_values, yerr=self.evpa_sigmas, marker="o",
                              color="0.5", markersize=1.5, linestyle="")
        self.ax_q.errorbar(self.times, self.q_values, yerr=self.q_sigmas, marker="o",
                           color="0.5", markersize=1.5, linestyle="")
        self.ax_u.errorbar(self.times, self.u_values, yerr=self.u_sigmas, marker="o",
                           color="0.5", markersize=1.5, linestyle="")
        for i in range(len(self.kinematics)):
            t = self.kinematics[i][0]
            dt = self.kinematics[i][1]
            self.ax_degree.axvspan(t-dt, t+dt, alpha=0.5, color="green")

    def plot_polarization_blocks(self):
        """
        Plot blocks of polarization on a given axis
        """
        for idx in range(len(self.block_edges)):
            edge_start = self.block_edges[idx][0]
            edge_end = self.block_edges[idx][1]
            x = [edge_start, edge_end]
            # Degree plot
            self.ax_degree.hlines(y=self.block_degree_values[idx], xmin=edge_start, xmax=edge_end, color="C0")
            y1 = [self.block_degree_values[idx]-self.block_degree_sigmas[idx],
                  self.block_degree_values[idx]-self.block_degree_sigmas[idx]]
            y2 = [self.block_degree_values[idx]+self.block_degree_sigmas[idx],
                  self.block_degree_values[idx]+self.block_degree_sigmas[idx]]
            self.ax_degree.fill_between(x, y1, y2, color="C0", alpha=0.5)
            if (idx != 0) and (idx != len(self.block_edges)-1):
                self.ax_degree.vlines(x=edge_start, ymin=self.block_degree_values[idx-1],
                                      ymax=self.block_degree_values[idx], linewidth=2)
                self.ax_degree.vlines(x=edge_end, ymin=self.block_degree_values[idx],
                                      ymax=self.block_degree_values[idx+1], linewidth=2)

        for idx in range(len(self.block_edges)):
            edge_start = self.block_edges[idx][0]
            edge_end = self.block_edges[idx][1]
            x = [edge_start, edge_end]
            # Check if this block is inside of found rotations
            color = "C0"
            if self.detected_rotations_inds:
                for i, rot_inds in enumerate(self.detected_rotations_inds):
                    if rot_inds[0] < idx <= rot_inds[1]:
                        if self.detected_rotations_directions[i] == 1:
                            color = "C1"
                        if self.detected_rotations_directions[i] == -1:
                            color = "C2"
            # Evpa degree
            self.ax_evpa.hlines(y=self.block_evpa_values[idx], xmin=edge_start, xmax=edge_end, color=color)
            y1 = [self.block_evpa_values[idx]-self.block_evpa_sigmas[idx],
                  self.block_evpa_values[idx]-self.block_evpa_sigmas[idx]]
            y2 = [self.block_evpa_values[idx]+self.block_evpa_sigmas[idx],
                  self.block_evpa_values[idx]+self.block_evpa_sigmas[idx]]
            self.ax_evpa.fill_between(x, y1, y2, color=color, alpha=0.5)
            if (idx != 0) and (idx != len(self.block_edges)-1):
                self.ax_evpa.vlines(x=edge_start, ymin=self.block_evpa_values[idx-1],
                                    ymax=self.block_evpa_values[idx], linewidth=2, color=color)
                self.ax_evpa.vlines(x=edge_end, ymin=self.block_evpa_values[idx],
                                    ymax=self.block_evpa_values[idx+1], linewidth=2, color=color)

        # Mark times where the +/- 180 degrees ambiguity were solved
        for idx in range(len(self.evpa_shift_times)):
            t = self.evpa_shift_times[idx][0]
            direction = self.evpa_shift_times[idx][1]
            if direction == 1:
                self.ax_evpa.axvline(x=t, color="g", alpha=0.5, linestyle="--")
            else:
                self.ax_evpa.axvline(x=t, color="r", alpha=0.5, linestyle="--")

    def plot_edges_and_fits(self, edges, fits):
        while self.edges_plot_instances:
            self.edges_plot_instances.pop().remove()
        for idx, (start, end) in enumerate(pairwise(edges)):
            p = self.ax_evpa.axvline(x=start, color="k", alpha=0.5, linestyle=":")
            self.edges_plot_instances.append(p)
            p = self.ax_evpa.axvline(x=end, color="k", alpha=0.5, linestyle=":")
            self.edges_plot_instances.append(p)
            x = [start, end]
            y = [linear_model(fits[idx], start), linear_model(fits[idx], end)]
            p = self.ax_evpa.plot(x, y, color="k")[0]
            self.edges_plot_instances.append(p)
        self.figure.canvas.draw()

    def plot_stokes_data(self):
        pass

    def find_rotations_continuous(self, min_blocks):
        """
        Find rotations in chunk as a set of blocks with the same sign of PA change. There
        should be at least min_blocks in the set
        """
        evpa_diffs = np.diff(self.block_evpa_values)  # Change to take sigmas into account
        evpa_directions = np.zeros_like(evpa_diffs)
        evpa_directions[evpa_diffs > 0] = 1
        evpa_directions[evpa_diffs < 0] = -1
        current_direction = evpa_directions[0]
        in_a_row = 1
        self.detected_rotations_inds = []
        self.detected_rotations_directions = []
        for idx in range(1, len(evpa_directions)):
            if evpa_directions[idx] == current_direction:
                in_a_row += 1
            else:
                if in_a_row >= min_blocks:
                    # Our rotations is long enough
                    rotation_start_idx = idx - in_a_row + 1
                    rotation_end_idx = idx
                    self.detected_rotations_inds.append((rotation_start_idx, rotation_end_idx))
                    self.detected_rotations_directions.append(current_direction)
                current_direction = evpa_directions[idx]
                in_a_row = 1

    def find_rotations_fraction(self, min_blocks, fraction):
        """
        Find rotations in chunk. A time range contains rotation inside it if a fraction of points with
        the same direction is higher that a given value
        """
        evpa_diffs = np.diff(self.block_evpa_values)  # Change to take sigmas into account
        evpa_directions = np.zeros_like(evpa_diffs)
        evpa_directions[evpa_diffs > 0] = 1
        evpa_directions[evpa_diffs < 0] = -1
        idx_center = 0
        iteration = -1
        left_shift = 0
        left_limit = 0
        right_shift = 0
        rotation_start_idx = None
        rotation_end_idx = None
        try_left = True
        try_right = True
        self.detected_rotations_inds = []
        self.detected_rotations_directions = []
        last_point = False
        while True:
            iteration += 1
            if iteration % 2 == 0:
                if idx_center + right_shift + 1 < len(evpa_directions) - 2:
                    # We can make a step in the right direction
                    if try_right:
                        right_shift += 1
                else:
                    # We have reached the right border of a chunk
                    right_shift += 1
                    last_point = True
                    try_right = False
            else:
                if idx_center - left_shift > left_limit:
                    # We can make a step in the left direction
                    if try_left:
                        left_shift += 1
                else:
                    try_left = False
                    continue
            total_points = right_shift + left_shift + 1
            to_check = evpa_directions[idx_center-left_shift:idx_center+right_shift+1]
            fraction_cw = len(np.where(to_check == 1)[0]) / total_points
            fraction_ccw = len(np.where(to_check == -1)[0]) / total_points
            print(f"{left_shift=} {idx_center=} {right_shift=} {fraction_cw=} {fraction_ccw}")
            if ((try_left is True) or (try_right is True)) and (total_points < min_blocks):
                # The chunk is too short for now. Let's allow it to grow a bit before making any computatons
                continue
            if (fraction_cw > fraction) or (fraction_ccw > fraction):
                # There is a rotation in a given part of a chunk
                rotation_start_idx = idx_center-left_shift
                rotation_end_idx = idx_center+right_shift
            else:
                if iteration % 2 == 0:
                    try_right = False
                else:
                    try_left = False
            print(f"----------------- {try_left=} {try_right=} {last_point=}")
            if ((try_left is False) and (try_right is False)) or (last_point is True):
                # No rotation on this part of a chunk, or a final point was included
                if rotation_start_idx is not None:
                    # There were a rotation on a previous step, so let's save it.
                    # But at first, check if we can strip it head and tail to remove
                    # points with another direction of a rotation
                    if np.mean(evpa_directions[rotation_start_idx: rotation_end_idx]) > 0:
                        average_direction = 1
                    else:
                        average_direction = -1
                    while 1:
                        # Strip left border
                        if evpa_directions[rotation_start_idx] != average_direction:
                            rotation_start_idx += 1
                        else:
                            break
                    while 1:
                        # Strip right border
                        if evpa_directions[rotation_end_idx] != average_direction:
                            rotation_end_idx -= 1
                        else:
                            break
                    self.detected_rotations_inds.append((rotation_start_idx, rotation_end_idx))
                    self.detected_rotations_directions.append(average_direction)
                if last_point is True:
                    break
                else:
                    if rotation_start_idx is not None:
                        idx_center += right_shift
                        left_limit = rotation_end_idx
                    else:
                        idx_center += 1
                    try_left = True
                    try_right = True
                    rotation_start_idx = None
                    rotation_end_idx = None
                    left_shift = 0
                    right_shift = 0
                    iteration = 0

    def find_rotations_binomial(self, min_blocks, max_p_value):
        evpa_diffs = np.diff(self.block_evpa_values)  # Change to take sigmas into account
        evpa_directions = np.zeros_like(evpa_diffs)
        evpa_directions[evpa_diffs > 0] = 1
        evpa_directions[evpa_diffs < 0] = -1
        self.detected_rotations_inds = []
        self.detected_rotations_directions = []
        if len(evpa_directions) < 4:
            return
        idx_center = 1
        rotation_inds = np.zeros_like(evpa_directions)
        min_p_values = np.ones_like(evpa_directions)
        for idx_center in range(1, len(evpa_directions)-1):
            # For some point find all possible time intervals around it, and for every such interval find
            # a probability of a rotation for a binomial distribution
            left_max_shift = idx_center
            right_max_shift = len(evpa_directions) - idx_center
            print(f"{left_max_shift=} {right_max_shift=}")
            for left_shift in range(0, left_max_shift+1):
                for right_shift in range(1, right_max_shift):
                    idx_start = idx_center - left_shift
                    idx_end = idx_center + right_shift
                    to_check = evpa_directions[idx_start:idx_end]
                    total_points = len(to_check)
                    if total_points < min_blocks:
                        continue
                    number_cw = len(np.where(to_check == 1)[0])
                    number_ccw = len(np.where(to_check == -1)[0])
                    number_same_direction = max(number_cw, number_ccw)
                    p_value = binomtest(k=number_same_direction, n=total_points, alternative='greater').pvalue
                    if p_value < min_p_values[idx_center]:
                        min_p_values[idx_center] = p_value
                    if p_value < max_p_value:
                        print(f"{idx_center=} {left_shift=} {right_shift=} {total_points=} {number_same_direction=} {p_value=}")
                        if np.mean(evpa_directions[idx_start: idx_end]) > 0:
                            rotation_inds[idx_start:idx_end] = 1
                        else:
                            rotation_inds[idx_start:idx_end] = -1
        # Now we have marked the days where rotations occure. Let's find the ranges of separate rotations
        in_cw_rotation = False
        in_ccw_rotation = False
        for idx in range(idx_start, len(rotation_inds)):
            if (rotation_inds[idx] == 1) and (in_cw_rotation is False) and (in_ccw_rotation is False):
                # There were no rotation before that, but now a clockwise rotation is starting
                in_cw_rotation = True
                start_idx = idx
            if (rotation_inds[idx] == 1) and (in_cw_rotation is False) and (in_ccw_rotation is True):
                # A counterclockwise rotation is replaced by a clockwise
                self.detected_rotations_inds.append((start_idx, idx))
                self.detected_rotations_directions.append(-1)
                in_cw_rotation = True
                in_ccw_rotation = False
                start_idx = idx
            if (rotation_inds[idx] == -1) and (in_cw_rotation is False) and (in_ccw_rotation is False):
                # There were no rotation before that, but now a counterclockwise is starting
                in_ccw_rotation = True
                start_idx = idx
            if (rotation_inds[idx] == -1) and (in_cw_rotation is True) and (in_ccw_rotation is False):
                # A clockwise rotation is replaced by a counterclockwise
                self.detected_rotations_inds.append((start_idx, idx))
                self.detected_rotations_directions.append(1)
                in_cw_rotation = False
                in_ccw_rotation = True
                start_idx = idx
            if (rotation_inds[idx] == 0) and (in_cw_rotation is True):
                # A clockwise rotation has finished
                self.detected_rotations_inds.append((start_idx, idx))
                self.detected_rotations_directions.append(1)
                in_cw_rotation = False
            if (rotation_inds[idx] == 0) and (in_ccw_rotation is True):
                # A counterclockwise rotation has finished
                self.detected_rotations_inds.append((start_idx, idx))
                self.detected_rotations_directions.append(-1)
                in_ccw_rotation = False
            if (rotation_inds[idx] == 0) and (in_cw_rotation is False) and (in_ccw_rotation is False):
                # No rotation were before and no at the current moment
                continue

    def investigate_selected_region_bb(self):
        """
        Compute various statistics about the selected region based on Bayesian blocks representation
        """
        # Plot general information on the region
        self.t_test_p_value = None
        self.binom_test_p_value = None
        print("\nSelected region:")
        print(f"Time range: {self.selected_region_start_pos:1.2f} -- {self.selected_region_end_pos:1.2f}")
        inds_selected = np.where((self.times > self.selected_region_start_pos) *
                                 (self.times < self.selected_region_end_pos))
        # times_selected = self.times[inds_selected]
        block_inds_selected = np.where((self.block_edges[:, 1] > self.selected_region_start_pos) *
                                       (self.block_edges[:, 0] < self.selected_region_end_pos))[0]
        if len(block_inds_selected) < 3:
            print("Selected block is too short")
            return
        evpa_selected = self.evpa_values[inds_selected]
        block_evpa_selected = self.block_evpa_values[block_inds_selected]

        # Find out about directions and rates of EVPA changes
        evpa_diffs = np.diff(self.block_evpa_values[block_inds_selected])  # Change to take sigmas into account
        evpa_directions = np.zeros_like(evpa_diffs)
        evpa_directions[evpa_diffs > 0] = 1
        evpa_directions[evpa_diffs < 0] = -1
        skipped_sigmas = []
        for skip_idx in range(len(block_inds_selected)):
            inds_skipped = list(block_inds_selected)
            inds_skipped.pop(skip_idx)
            evpa_rates = (np.diff(self.block_evpa_values[inds_skipped]) /
                          np.diff(self.block_centers[inds_skipped]))
            evpa_rate_sigmas = np.array([(s1**2+s2**2)**0.5
                                         for (s1, s2) in pairwise(self.block_evpa_sigmas[inds_skipped])])
            skipped_sigmas.append(np.std(evpa_rates))
        skipped_sigmas = np.array(skipped_sigmas)
        skipped_sigmas_mean = np.mean(skipped_sigmas)
        skipped_sigmas_std = np.std(skipped_sigmas)
        points_to_skip = np.where(skipped_sigmas < skipped_sigmas_mean - 3*skipped_sigmas_std)

        skipped_centers = self.block_centers[block_inds_selected[points_to_skip]]
        skipped_evpa = self.block_evpa_values[block_inds_selected[points_to_skip]]
        self.plot_skipped(skipped_centers, skipped_evpa)

        block_inds_selected = np.delete(block_inds_selected, points_to_skip)
        evpa_rates = (np.diff(self.block_evpa_values[block_inds_selected]) /
                      np.diff(self.block_centers[block_inds_selected]))
        evpa_rate_sigmas = np.array([(s1**2+s2**2)**0.5
                                     for (s1, s2) in pairwise(self.block_evpa_sigmas[block_inds_selected])])
        evpa_rate_sigmas /= np.diff(self.block_centers[block_inds_selected])

        self.mean_rate_w = np.sum(evpa_rates/evpa_rate_sigmas**2) / (np.sum(1/evpa_rate_sigmas**2))
        N = len(evpa_rates)
        num = np.sum((evpa_rates - self.mean_rate_w) ** 2 / evpa_rate_sigmas**2)
        denum = (N-1) * np.sum(1/evpa_rate_sigmas**2) / N
        sigma_rate_w = (num/denum) ** 0.5
        t_statistics = self.mean_rate_w / (sigma_rate_w/(N**0.5))
        self.t_test_p_value = 2 * (1 - stats.t.cdf(abs(t_statistics), N))
        print(f"  Weighted t-test p_value: {bcolors.OKBLUE}{bcolors.BOLD} {self.t_test_p_value:1.4f} {bcolors.ENDC} \n")

        # print(f"Evpa measurements: {len(evpa_selected)}, grouped in {len(block_evpa_selected)} blocks")

        # Find median block length
        t_start = self.block_edges[block_inds_selected][1][0]
        t_end = self.block_edges[block_inds_selected][-1][1]-0.0001
        n_tests = len(block_inds_selected)
        # print(f"{n_tests=}")
        sampled_directions = []
        for t in np.linspace(t_start, t_end, n_tests):
            # Find a block that contains this time
            for block_idx in range(len(self.block_edges)):
                if (self.block_edges[block_idx][0] <= t) and (self.block_edges[block_idx][1] > t):
                    sampled_directions.append(self.block_evpa_directions[block_idx])
                    break
            else:
                if (self.block_edges[-1][0] < t):
                    sampled_directions.append(self.block_evpa_directions[-1])
        sampled_directions = np.array(sampled_directions)

        number_cw = len(np.where(sampled_directions > 0)[0])
        number_ccw = len(np.where(sampled_directions < 0)[0])
        print(f"  There are {len(evpa_diffs)} EVPA changes among which")
        print(f"  {number_cw} are clockwise")
        print(f"  {number_ccw} are counterclockwise")
        # Compute statistics
        self.binom_test_p_value = binomtest(k=max(number_cw, number_ccw),
                                            n=number_cw+number_ccw,
                                            alternative='greater').pvalue
        print(f"  Binomial p-value statistic:{bcolors.OKBLUE}{bcolors.BOLD} {self.binom_test_p_value:1.4f} {bcolors.ENDC}")

    def investigate_selected_region(self):
        """
        Compute various statistics about the selected region based on Bayesian blocks representation
        """
        # Plot general information on the region
        inds_selected = np.where((self.times > self.selected_region_start_pos) *
                                 (self.times < self.selected_region_end_pos))
        times_selected = self.times[inds_selected]
        t_min = times_selected[0]
        t_max = times_selected[-1]
        evpa_selected = self.evpa_values[inds_selected]
        evpa_sigmas_selected = self.evpa_sigmas[inds_selected]
        print("\nSelected region:")
        print(f"Time range: {t_min} -- {t_max}")
        print(f"Evpa measurements: {len(evpa_selected)}, grouped in blocks")
        # Find a splitting of the interval, such that at least two points are in each sub interval
        n_split = 2
        while 1:
            edges = np.linspace(t_min, t_max, n_split)
            min_points_inside = 1e3
            for start, end in pairwise(edges):
                inds_inside = np.where((times_selected >= start) * (times_selected <= end))
                if len(inds_inside[0]) < min_points_inside:
                    min_points_inside = len(inds_inside[0])
            if min_points_inside < 3:
                best_split = n_split - 1
                break
            n_split += 1
        # Find slopes for each interval
        edges = np.linspace(t_min, t_max, best_split)
        fits = []
        for start, end in pairwise(edges):
            inds_inside = np.where((times_selected >= start) * (times_selected <= end))
            times_inside = times_selected[inds_inside]
            evpa_inside = evpa_selected[inds_inside]
            evpa_sigmas_inside = evpa_sigmas_selected[inds_inside]
            fit_res = fit_by_line(times_inside, evpa_inside, evpa_sigmas_inside)
            fits.append(fit_res.beta)
        self.plot_edges_and_fits(edges, fits)


def main(args):
    # Load kinematic parameters
    kinematics = []
    if args.kinematics is not None:
        for line in open(args.kinematics):
            if line.startswith("#"):
                continue
            params = line.split()
            if len(params) > 11:
                t = float(params[11])
                dt = float(params[12])
                kinematics.append((Time(t, format="decimalyear").mjd, dt*365.2425))

    if not args.outdir.exists():
        os.makedirs(args.outdir)
    objname = os.path.splitext(args.datafile.name)[0]
    # Load data from file
    times = []
    magnitude_values = []
    magnitude_sigmas = []
    degree = []
    degree_sigmas = []
    evpa_values = []
    evpa_sigmas = []
    for line in open(args.datafile):
        if line.startswith("#"):
            continue
        source = line.split()[7].lower()
        if source not in allowed_sources:
            continue
        times.append(float(line.split()[0]))
        magnitude_values.append(float(line.split()[1]))
        magnitude_sigmas.append(float(line.split()[2]))
        degree.append(float(line.split()[3]))
        degree_sigmas.append(float(line.split()[4]))
        evpa_values.append(float(line.split()[5]))
        evpa_sigmas.append(float(line.split()[6]))
    times = np.array(times)
    magnitude_values = np.array(magnitude_values)
    magnitude_sigmas = np.array(magnitude_sigmas)
    degree_values = np.array(degree)
    degree_sigmas = np.array(degree_sigmas)
    evpa_values = np.array(evpa_values)
    evpa_sigmas = np.array(evpa_sigmas)

    # Average points that are closer than some given value. If such a value is not given,
    # average only points that have the same measurement times
    # Convert magnitudes to fluxes
    m0 = 25
    flux_values = 10**(0.4 * (m0-magnitude_values))
    flux_sigmas = 0.4 * np.log(10) * flux_values * magnitude_sigmas
    flux_weights = 1 / flux_sigmas**2
    degree_weights = 1 / degree_sigmas**2
    evpa_weights = 1 / evpa_sigmas**2
    idx = 0
    groups = []  # Group close points together
    while idx < len(times):
        inds_close = np.where((times >= times[idx]) * (times < times[idx] + args.min_time_bin))[0]
        groups.append(inds_close)
        idx += len(inds_close)

    # Average values indide groups
    times_clean = []
    flux_values_clean = []
    flux_sigmas_clean = []
    degree_values_clean = []
    degree_sigmas_clean = []
    evpa_values_clean = []
    evpa_sigmas_clean = []
    for group in groups:
        times_clean.append(np.mean(times[group]))
        flux_values_grouped = flux_values[group]
        flux_weights_grouped = flux_weights[group]
        good_inds = np.where(np.isfinite(flux_values_grouped))
        if len(good_inds[0]):
            flux_values_clean.append(np.average(flux_values_grouped[good_inds],
                                                weights=flux_weights_grouped[good_inds]))
            flux_sigmas_clean.append(np.sqrt(1/np.sum(flux_weights_grouped[good_inds])))
        else:
            flux_values_clean.append(np.nan)
            flux_sigmas_clean.append(np.nan)
        degree_values_clean.append(np.average(degree_values[group], weights=degree_weights[group]))
        degree_sigmas_clean.append(np.sqrt(1/np.sum(degree_weights[group])))
        evpa_values_clean.append(np.average(evpa_values[group], weights=evpa_weights[group]))
        evpa_sigmas_clean.append(np.sqrt(1/np.sum(evpa_weights[group])))
    times = np.array(times_clean)
    magnitude_values = -2.5 * np.log10(flux_values_clean) + m0
    magnitude_sigmas = 2.5 * np.array(flux_sigmas_clean) / (np.log(10) * np.array(flux_values_clean))
    degree_values = np.array(degree_values_clean)
    degree_sigmas = np.array(degree_sigmas_clean)
    evpa_values = np.array(evpa_values_clean)
    evpa_sigmas = np.array(evpa_sigmas_clean)

    # Filter big errors
    if args.max_degree_relative_error is not None:
        # Filter points with big errors in the polarization degree
        degree_relative_sigma = degree_sigmas / degree_values
        good_inds = np.where(degree_relative_sigma < args.max_degree_relative_error)
        times = times[good_inds]
        magnitude_values = magnitude_values[good_inds]
        magnitude_sigmas = magnitude_sigmas[good_inds]
        degree_values = degree_values[good_inds]
        degree_sigmas = degree_sigmas[good_inds]
        evpa_values = evpa_values[good_inds]
        evpa_sigmas = evpa_sigmas[good_inds]

    if args.max_evpa_error is not None:
        # Filter points with big errors in the EVPA
        good_inds = np.where(evpa_sigmas < args.max_evpa_error)
        times = times[good_inds]
        magnitude_values = magnitude_values[good_inds]
        magnitude_sigmas = magnitude_sigmas[good_inds]
        degree_values = degree_values[good_inds]
        degree_sigmas = degree_sigmas[good_inds]
        evpa_values = evpa_values[good_inds]
        evpa_sigmas = evpa_sigmas[good_inds]

    # Split the engire light curve on chunks
    chunks = []
    last_gap_position = times[0]
    for idx in range(1, len(evpa_values)):
        mjd_delta = times[idx] - times[idx-1]
        if mjd_delta > args.max_gap_size:
            # The time difference between two points is too big. Let's separate this group
            # of measuremetns into a separate chunk
            chunk_inds = np.where((times >= last_gap_position) * (times < times[idx]))
            if len(chunk_inds[0]) > 2:
                # Skip short chunks
                chunk = PolarizationChunk(times=times[chunk_inds],
                                          magnitude_values=magnitude_values[chunk_inds],
                                          magnitude_sigmas=magnitude_sigmas[chunk_inds],
                                          degree_values=degree_values[chunk_inds],
                                          degree_sigmas=degree_sigmas[chunk_inds],
                                          evpa_values=evpa_values[chunk_inds],
                                          evpa_sigmas=evpa_sigmas[chunk_inds],
                                          kinematics=kinematics)
                chunks.append(chunk)
            last_gap_position = times[idx]
    # Add the last chunk
    chunk_inds = np.where((times >= last_gap_position))
    if len(chunk_inds[0]) > 2:
        # Skip short chunks
        chunk = PolarizationChunk(times=times[chunk_inds],
                                  magnitude_values=magnitude_values[chunk_inds],
                                  magnitude_sigmas=magnitude_sigmas[chunk_inds],
                                  degree_values=degree_values[chunk_inds],
                                  degree_sigmas=degree_sigmas[chunk_inds],
                                  evpa_values=evpa_values[chunk_inds],
                                  evpa_sigmas=evpa_sigmas[chunk_inds],
                                  kinematics=kinematics)
        chunks.append(chunk)

    detected_rotations = 0
    while chunks:
        chunk = chunks.pop(0)
        chunk.compute_blocks(0.05)
        if len(chunk.block_degree_values) < 6:
            continue
        # chunk.find_rotations_binomial(6, 0.05)
        chunk.setup_plots(objname)
        chunk.plot_data()
        chunk.plot_polarization_blocks()
        chunk.show()
        input()
        chunk.clean_plots()
        if chunk.first_sub_chunk is not None:
            chunks.insert(0, chunk.second_sub_chunk)
            chunks.insert(0, chunk.first_sub_chunk)
        else:
            for i in range(len(chunk.saved_rotations_edges)):
                fout = args.outdir / f"{objname}_rotation_{detected_rotations}.json"
                chunk.save_selection_to_file(i, fout)
                detected_rotations += 1



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--datafile", type=Path,
                        help="Path to file with polarization data")
    parser.add_argument("--max-evpa-error", type=float, default=None,
                        help="Filter points where EVPA error is bigger than the given value")
    parser.add_argument("--max-degree-relative-error", type=float, default=None,
                        help="Filter points with the relative polarization degree error is bigger than the given value")
    parser.add_argument("--max-gap-size", type=float, default=90,
                        help="Reset cumulative EVPA value if gap between points is bigger then that")
    parser.add_argument("--min-time-bin", type=float, default=0.001,
                        help="Average measurements that are closer than this value")
    parser.add_argument("--outdir", type=Path, help="Name of directory to save selected regions")
    parser.add_argument("--kinematics", type=Path, help="File with outflows parameters")
    args = parser.parse_args()
    main(args)
